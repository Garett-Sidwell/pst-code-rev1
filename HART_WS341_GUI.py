# -*- coding: utf-8 -*-
"""
@author: dthompson, Norgren Leeds Ltd 9/8/2020
GUI to help perform basic tasks for WS341 project
"""

import sys
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QPushButton, QWidget, QLineEdit, \
            QCheckBox, QFormLayout, QVBoxLayout, QGroupBox, QMainWindow, \
            QComboBox, QMessageBox
from PyQt5.QtCore import QTimer
from hartlib.HART_WS341 import HART_WS341
import traceback
import serial.tools.list_ports

KNOWN_HART_MODEM_SERIAL_NUMBERS = [
    "AJ02XW7TA",
	"AQ00EZFKA",
]
attr_port = "port"
attr_device = "device"
class WS341_HARTTestConsole(QMainWindow):
    
    dev = None
    excel = None
    dev = None
    
    def __init__(self, comport=None):
        super().__init__()
        self.initComPortList()
        self.initUI()
        self.doInitComms()

    comportslist = []
    comportslistdefault = 0
    
    def initComPortList(self, default=None):
        """ List the available comm ports and work out the index of the first one
        we think is a HART modem"""
        self.comportslist = []
        for port in serial.tools.list_ports.comports():
            self.comportslist.append([port, port.device])
        self.comportslist.sort()
        for i, (port, device) in enumerate(self.comportslist):
            #print(f"sn={port.serial_number}")
            if port.serial_number in KNOWN_HART_MODEM_SERIAL_NUMBERS:
                self.comportslist[i][1] = device + " (HART modem)"
                if default is None:
                    self.comportslistdefault = i
            if device is default:
                self.comportslistdefault = i
        
    def initComms(self, comport):
        self.dev = HART_WS341(comport.device)
        self.dev.DEBUG_HART_TXRX = False
        
    def initUI(self):               
        centralWidget = QWidget(self)  
        self.setCentralWidget(centralWidget)
        
        mainLayout = QVBoxLayout()
        #############
        groupBox = QGroupBox("Connection")
        layout = QFormLayout()

        combo = QComboBox()
        self.initComPortList()
        for port, device in self.comportslist:
            combo.addItem(device)
        if self.comportslistdefault is not None:
            combo.setCurrentIndex(self.comportslistdefault)
        self.comPortCombo = combo
        layout.addRow(combo)
        
        btn = QPushButton('Connect', self)
        btn.clicked.connect(self.doInitComms)
        layout.addRow(btn)
        self.connectButton = btn
        
        btn = QPushButton('Disconnect', self)
        btn.clicked.connect(self.uutDisconnected)
        btn.setDisabled(True)
        layout.addRow(btn)
        self.disconnectButton = btn

        groupBox.setLayout(layout)
        mainLayout.addWidget(groupBox)

        #####################
        groupBoxPData = QGroupBox("Process Variables")
        layout = QFormLayout()

        box = QLineEdit()
        self.pPositionBox = box
        box.setReadOnly(True)
        layout.addRow("Position", box)

        box = QLineEdit()
        box.setReadOnly(True)
        self.pPressureBox = box
        layout.addRow("Pressure", box )

        box = QLineEdit()
        box.setReadOnly(True)
        self.temperatureBox = box
        layout.addRow("CPU Temperature", box)

        btn = QPushButton("Update")
        btn.clicked.connect(self.updatePData)
        layout.addRow(None, btn)

        groupBoxPData.setLayout(layout)        
        self.groupBoxPData = groupBoxPData
        mainLayout.addWidget(groupBoxPData)
        
        #############
        groupBoxParam = QGroupBox("Parameters")
        layout = QFormLayout()

        groupBoxParam.setLayout(layout)
        self.groupBoxParam = groupBoxParam

        mainLayout.addWidget(groupBoxParam)
        
        #############
        groupBoxTools  = QGroupBox("Tools")
        layout = QVBoxLayout()
        
        btn = QPushButton('Save Tracedata to Excel', self)
        btn.clicked.connect(self.doCopyDataToExcel)
        layout.addWidget(btn)

        groupBoxTools.setLayout(layout)
        self.groupBoxTools = groupBoxTools
        
        mainLayout.addWidget(groupBoxTools)

        #############
        groupBox = QGroupBox("Info")
        layout = QFormLayout()

        box = QLineEdit()
        box.setReadOnly(True)
        layout.addRow("SN", box)
        self.snBox = box
        
        box = QLineEdit()
        box.setReadOnly(True)
        layout.addRow("FW", box)
        self.fwBox = box

        box = QLineEdit()
        box.setReadOnly(True)
        layout.addRow("HW", box)
        self.hwBox = box

        groupBox.setLayout(layout)
        mainLayout.addWidget(groupBox)
        self.groupBoxInfo = groupBox

        centralWidget.setLayout(mainLayout)
        self.setWindowTitle("WS341 HART")

        self.timer = QTimer(self)
        self.timer.setInterval(1000)          # interval 1000ms
        self.timer.timeout.connect(self.updatePData)  
        self.timer.start()
        self.show()

    def uutConnected(self):
        print("Connected.")
        self.groupBoxPData.setDisabled(False)
        self.groupBoxTools.setDisabled(False)
        self.groupBoxInfo.setDisabled(False)
        self.updateInfo()
        self.updateParams()
        self.connectButton.setDisabled(True)
        self.disconnectButton.setDisabled(False)
    
    def uutDisconnected(self):
        if self.dev:
            self.dev.close()
        self.dev = None
        self.groupBoxPData.setDisabled(True)
        self.groupBoxTools.setDisabled(True)
        self.groupBoxInfo.setDisabled(True)
        self.connectButton.setDisabled(False)
        self.disconnectButton.setDisabled(True)
   
    def doInitComms(self):
        try:
            i = self.comPortCombo.currentIndex()
            port, _ = self.comportslist[i]
            print("Connecting to %s..." % (port.device))
            self.initComms(port)
            self.uutConnected()
        except Exception as e:  # We assume that any exception here is due to dropping comms
            print(traceback.format_exc())                
            print(e)
            #QMessageBox.critical(self, "Error", str(e))
            self.uutDisconnected()
        
    def updatePData(self):
        if self.dev:
            try:
                resp = self.dev.SendCommand3_ReadDynamicVariables()
                self.pPositionBox.setText("%#.1f" % resp.PrimaryVariable)
                self.pPressureBox.setText("%#.4f" % resp.SecondaryVariable)
                self.temperatureBox.setText("%#.1f" % resp.TertiaryVariable)
            except Exception as e:  # We assume that any exception here is due to dropping comms
                print(traceback.format_exc())      
                print(e)
                self.uutDisconnected()
        pass

    def updateInfo(self):
        if self.dev:
            try:
                ret = self.dev.readSerialNumbers()
                fw = ret.get("fwVer", "?")
                sn = ret.get("serialNum", "?")
                ret = self.dev.SendCommand0_ReadUniqueIdentifier()
                hw = str(ret.HardwareRev)
                self.snBox.setText(sn)
                self.fwBox.setText(fw)
                self.hwBox.setText(hw)
            except Exception as e:  # We assume that any exception here is due to dropping comms
                print(traceback.format_exc())                
                print(e)
                self.uutDisconnected()
        pass

    def updateParams(self):
        pass
        
    def doCopyDataToExcel(self):
        print("Starting test sequence")
        d = self.dev
        try: 
            if self.excel == None:   #create the sheet if not already created.
                # self.excel = Excel()
                self.excel.SaveAs("combination1.xlsx")
                
            mem = d.SendCommand143_GetMemoryStatus()
            print("Found %d traces, copying to excel..." % mem.UsedBlocks)
            for t in range(mem.UsedBlocks):
                sheet = "%d" % t
                self.excel.xl.Worksheets.Add()
                self.excel.xl.Activesheet.Name = sheet
                (resp, buf) = d.readTrace(t)
                for i, (name,value) in enumerate(resp._asdict().items()):
                    self.excel.writeData([name, value], sheet)
                self.excel.writeDataHeader(["time", "position", "pressure"], sheet)
                self.excel.writeData([])
                f = resp.sampleRate
                time = 0.0
                for n, row in enumerate(buf):
                    
                    self.excel.writeData([n*1/f] + row, sheet)
                
            #self.excel.writeData([1,2,3])
            #self.excel.Save()
        except:
            print(traceback.format_exc())
        finally:
            for e in equipment:
                e.close()
        print("Test Sequence Complete")

    def __del__(self):
        print("Closing...")
        if self.dev:
            print("Closing comport")
            self.dev.close()        
            
def launchGUI(comport=None):
    mainWin = None
    print("Python script started")
    app = QApplication(sys.argv)
    mainWin = WS341_HARTTestConsole(comport)
    app.aboutToQuit.connect(mainWin.__del__)
    mainWin.show()
    try:
        get_ipython().__class__.__name__
        __IPYTHON__ = True
    except NameError:
        __IPYTHON__ = False
    
    if __IPYTHON__:
        print("Running in IPYTHON compatibility mode")
        app.exec_()
    else:
        print("Running in standard mode")
        sys.exit(app.exec_())
    print("Python script finished")

GUI_MODE=True

##########################################################################
# Example usage:
if __name__ == "__main__":
    launchGUI(None)
