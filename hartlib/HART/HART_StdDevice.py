# -*- coding: utf-8 -*-
"""
Python implementation of HART Master stack.

Fairly minimal so far.  Not all commands implemented.

Basic standalone usage (just checks for valid 2-way comms):

python.exe HART_StdDevice.py [COMPORT]
where:
    COMPORT (optional) - the serial port where the HART modem is.
                        If ommitted, it will try to autodetect.
e.g.
python.exe HART_StdDevice.py COM6

Copyright 2020 Norgren Ltd

Created on Fri Aug  7 21:07:17 2020
$Id: HART_StdDevice.py 254 2021-02-23 16:39:16Z dthompson $
@author: dthompson, IMI Norgren Ltd, 9/8/2020
"""
# %%
import serial
import serial.tools.list_ports
from serial.serialutil import SerialException
from enum import IntEnum
import time
from datetime import datetime
from collections import namedtuple
import struct
#%%

NUM_TX_PREAMBLES = 5 # See HCF-SPEC054 section 3
NUM_RX_PREAMBLES_MAX = 20 # See HCF-SPEC054 section 3
# Duration of each byte transmitted on the line.  1200bps, 1start+8data+1stop+1parity
BYTE_TX_LENGTH_S = (1.0/1200)*(1+8+1+1)
# %%
class HARTException(Exception):
    """
    Base class for HART exceptions
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    
class HARTInvalidCommandException(HARTException):
    pass

class HARTInvalidParameterException(HARTException):
    pass

class HARTInvalidAddressException(HARTException):
    pass

class HARTDataTooLongException(HARTException):
    pass

class HARTNotImplementedException(HARTException):
    pass

class HARTRxException(HARTException):
    pass

class HARTRxTimeoutException(HARTRxException):
    pass

class HARTRxCommandNotImplementedException(HARTRxException):
    pass

class HARTRxTooFewDataBytesException(HARTRxException):
    pass

class HARTRxInvalidSelectionException(HARTRxException):
    pass

class HARTRxAccessRestrictedException(HARTRxException):
    pass


class HARTTimeSetCode(IntEnum):
    ReadReceiveTime = 0
    WriteDateAndTime = 1
    
class HARTFrameType(IntEnum):
    BACK = 1
    STX = 2
    ACK = 6

class HARTPhysicalLayerType(IntEnum):
    Asynchronous = 0
    Synchronous = 1
    
class HARTAddressType(IntEnum):
    Polling = 0  # 1 byte address
    Unique = 1   # 5 byte address

class HARTUnits(IntEnum):
    """Units as defined in HCF_SPEC-183"""
    PSI = 6
    bar = 7
    degC = 32
    degF = 33
    percent = 57

class HARTDate:
    def __init__(self, dateraw: bytes):
        if len(dateraw) != 3:
            raise HARTInvalidParameterException("HART date requires 3 bytes, "
                                                "not %d" % len(dateraw))
    def __str__(self):
        return "dateplaceholder"

    def __repr__(self):
        return "dateplaceholder"
    
class HARTTime:
    def __init__(self, time: bytes):
        if len(time) != 4:
            raise HARTInvalidParameterException("HART time requires 4 bytes, "
                                                "not %d" % len(time))
    def __repr__(self):
        return "timeplaceholder"
    def __str__(self):
        return "timeplaceholder"

class HARTDeviceStatus:
    """
    Class for decoding the device status byte returned by a hart device.
    """
    def __init__(self, devstatus=0):
        self.value = devstatus
        self.DeviceMalfunction             = bool(devstatus & 0x80)
        self.ConfigurationChanged          = bool(devstatus & 0x40)
        self.ColdStart                     = bool(devstatus & 0x20)
        self.MoreStatusAvailable           = bool(devstatus & 0x10)
        self.LoopCurrentFixed              = bool(devstatus & 0x08)
        self.LoopCurrentSaturated          = bool(devstatus & 0x04)
        self.NonPrimaryVariableOutOfLimits = bool(devstatus & 0x02)
        self.PrimaryVariableOutOfLimits    = bool(devstatus & 0x01)

    def aslist(self):
        a = []
        if self.DeviceMalfunction:
            a.append("DEVICE_MALFUNCTION")
        if self.ConfigurationChanged:
            a.append("CONFIGURATION_CHANGED")
        if self.ColdStart:
            a.append("COLD_START")
        if self.MoreStatusAvailable:
            a.append("MORE_STATUS_AVAILABLE")
        if self.LoopCurrentFixed:
            a.append("LOOP_CURRENT_FIXED")
        if self.LoopCurrentSaturated:
            a.append("LOOP_CURRENT_SATURATED")
        if self.NonPrimaryVariableOutOfLimits:
            a.append("NON_PRIMARY_VARIABLE_OUT_OF_LIMITS")
        if self.PrimaryVariableOutOfLimits:
            a.append("PRIMARY_VARIABLE_OUT_OF_LIMITS")
        return a

    def __repr__(self):
        a = self.aslist()
        if len(a) > 0:
            return ('<DeviceStatus: %s (%s)>' % 
                (hex(self.value), ', '.join(a)))
        else:
            return '<DeviceStatus: %s>' % hex(self.value)
        
    def __str__(self):
        a = self.aslist()
        if len(a) > 0:
            return ('Device Status: %s (%s)' % 
                (hex(self.value), ', '.join(a)))
        else:
            return 'Device Status: %s' % hex(self.value)

    
class HARTCommunicationStatus:
    """
    Class for decoding the comms status byte returned by a hart device.
    """
    def __init__(self, commstatus=0):
        self.value = commstatus
        self.CommunicationError      = bool(commstatus & 0x80)
        if not self.CommunicationError:
            # If there is no communication error flag, the rest of the
            # bytes represent the response code (not the codes below), so
            # mask them out (See HCF spec085 section 8.1.1)
            commstatus &= 0b10000000            
        self.VerticalParityError     = bool(commstatus & 0x40)
        self.OverrunError            = bool(commstatus & 0x20)
        self.FramingError            = bool(commstatus & 0x10)
        self.LongitudinalParityError = bool(commstatus & 0x08)
        self.CommunicationFailure    = bool(commstatus & 0x04)
        self.BufferOverflow          = bool(commstatus & 0x02)
        self.Reserved                = bool(commstatus & 0x01)

    def aslist(self):
        a = []
        if self.CommunicationError:
            a.append("COMMUNICATION_ERROR")
        if self.VerticalParityError:
            a.append("VERTICAL_PARTIY_ERROR")
        if self.OverrunError:
            a.append("OVERRUN_ERROR")
        if self.FramingError:
            a.append("FRAMING_ERROR")
        if self.LongitudinalParityError:
            a.append("LONGITUDINAL_PARITY_ERROR")
        if self.CommunicationFailure:
            a.append("COMMUNICATION_FAILURE")
        if self.BufferOverflow:
            a.append("BUFFER_OVERFLOW")
        if self.Reserved:
            a.append("RESERVED")
        return a

    def __repr__(self):
        a = self.aslist()
        if len(a) > 0:
            return ('<CommunicationStatus: %s (%s)>' % 
                (hex(self.value), ', '.join(a)))
        else:
            return '<CommunicationStatus: %s>' % hex(self.value)
        
    def __str__(self):
        a = self.aslist()
        if len(a) > 0:
            return ('Communication Status: %s (%s)' % 
                (hex(self.value), ', '.join(a)))
        else:
            return 'Communication Status: %s (OK)' % hex(self.value)
        
class HARTResponseCode:
    """
    Class to help decode a respone code into a string.
    See HCF Spec037 section 6.
    Some codes are 'single definition error', so we can just display a string.
    Others are 'multi definition error', that means the string to display is 
    dependent on the command.
    """
    def __init__(self, code, command=None):
        self.value = code
        
    def __str__(self):
        """
        If someone wants the value in a str context, return a string.
        """
        c = self.value
        s = ""
        if c == 0:
            s = "Success"
        elif c == 1:
            s = "Undefined"
        elif c == 2:
            s = "Invalid selection"
        elif c == 5:
            s = "Too few data bytes received"
        elif c == 64:
            s = "Command not implemented"
        elif c == None:
            return "None"
        else:
            s = "???"
        return "%d (%s)" % (c, s)
    
    def __eq__(self, val):
        """Handle equality checking as int comparison"""
        return self.value == val

    def __int__(self):
        """If someone wants the value in an int context, return an int."""
        return self.value
    
class HARTCommandFrame:
    """
    Class for forming HART command packets, see HCF_SPEC-081 for details.
    """
    raw = b'' # bytearray
    delimiter = 0
    address = None
    command = None
    _bytecount = 0
    data = None
    checksum = 0
    
    def __init__(self, address, command, data = []):
        # sanity checks
        if address is None:
            raise HARTInvalidAddressException("Cannot send to address 'None'. "
                "(Perhaps you have not sent a command 0 to this device to "
                "learn it's unique ID yet?)")
        if command < 0:
            raise HARTInvalidCommandException("Invalid Command %d" % command)
        if command > 255:
             raise HARTNotImplementedException("HART command numbers "
                     "255-65536 are not suppported yet.")   
        if command != 0 and (address <= 255):
            raise HARTInvalidAddressException("Cannot use a polling address "
                "(i.e. address<256) for non-zero commands.")
        if data and len(data) > 255:
            raise HARTDataTooLongException("Data too long (%d)" % len(data))

        self.address = address
        self.command = command
        self.data = data
        if data:
            self.bytecount = len(data)
        else:
            self.bytecount = 0
        
        raw = b''
        raw += bytes([0xFF] * NUM_TX_PREAMBLES)
        #construct delimiter according to HCF_SPEC-081 5.1.1
        delimiter = 0
        delimiter += HARTFrameType.STX
        delimiter += (HARTPhysicalLayerType.Asynchronous << 3) 
        delimiter += 0 # number of expansion bytes
        addresstype = HARTAddressType.Polling if address <= 255 else HARTAddressType.Unique
        delimiter += addresstype << 7
        self.delimiter = delimiter
        
        raw += bytes([delimiter])
        if addresstype == HARTAddressType.Polling:
            raw += address.to_bytes(1, 'big')
        else:
            a = address.to_bytes(5, 'big')
            raw += a

        raw += bytes([command])
        # @todo sanitiy checks on command and data here (length, bounds).
        if data:
            raw += bytes([len(data)])     # append the byte count
            raw += bytes(data)            # append the data if there is any.
        else:
            raw += bytes([0])             # zero byte count
        self.checksum = 0
        # checksum calc'd on all but preamble
        for d in raw[NUM_TX_PREAMBLES:]: 
            self.checksum ^= d
        raw += bytes([self.checksum]) # append the checksum
        self.raw = raw
        
    def __str__(self):
        return("HART Command Frame:\n"
               " Delimiter: %s\n"
               " Address: %s\n"
               " Command: %s\n"
               " Byte Count: %s\n"
               " Data: %s\n"
               " Checksum: %s\n"
               " Raw: [%s]"
               % (hex(self.delimiter), hex(self.address), self.command, 
                self.bytecount, 
                ' '.join('{:02x}'.format(int(x)) for x in self.data),
                hex(self.checksum), 
                ' '.join('{:02x}'.format(int(x)) for x in list(self.raw))))

        
    
class HARTResponseFrame:
    """
    Class for decoding HART response packets, see HCF_SPEC-081 for details.
    """
    raw = b'' # bytearray
    
    def __init__(self, raw: bytearray):
        """
        Create a hart response frame object from a raw bytearray including preamble
        """
        #print(' '.join('{:02x}'.format(int(x)) for x in list(raw)))
        self.raw = raw
        preamblestarted = False
        for i in range(NUM_RX_PREAMBLES_MAX+1):
            # look out for start of preamble
            if not preamblestarted:
                if raw[i] == 0xFF:
                    preamblestarted = True
            else:
                # exit if preamble has started and now finished.
                if raw[i] != 0xFF:
                    break
        if raw[i] == 0xFF:
            raise HARTException("Preamble too long?")
        raw = raw[i:] # from this point on dont need the preamble.
        
        i=0
        self.delimiter = raw[i]
        i +=1
        if self.delimiter & 0x80: # HARTAddressType.Unique 
            self.address = struct.unpack(">Q",bytearray(3) + raw[i:i+5])[0] # 3 padding + 5 bytes
            i+=5
        else: # HARTAddressType.Polling
            self.address = raw[i] # 1 byte
            i+=1
        if self.delimiter & 0b01100000:
            raise HARTNotImplementedException("Expansion bytes support not "
                "implemented (delimeter=%s)" % hex(self.delimiter))
        self.command = raw[i]
        i+=1
        self.bytecount = raw[i]
        i+=1

        # next 2 bytes are comms status and response code (not part of the data)
        self.data = raw[i+2:i+self.bytecount]
        
        # Data field format is dependent the type of response.  
        # See HCF spec085 section 8.1
        self.communicationstatus = HARTCommunicationStatus(raw[i])
        commerr = self.communicationstatus.CommunicationError
        # if there was a comm error we dont get a response code, but if there
        # wasnt, read the 7LSBs of the commstatus byte to get the RC.
        rc = None if commerr else raw[i] & 0b01111111 
        self.responsecode = HARTResponseCode(rc)
        self.devicestatus = HARTDeviceStatus(raw[i+1])

        i+=self.bytecount
        self.checksum = raw[i]
        check = 0
        for d in raw:
            check ^= d
        if check != 0:
            print("Invalid checksum on Rx packet!")
            

    def __str__(self):
        return("HART Response Frame:\n"
               " Delimiter: %s\n"
               " Address: %s\n"
               " Command: %s\n"
               " Byte Count: %s\n"
               " %s\n"
               " %s\n"
               " ResponseCode: %s\n"
               " Data: %s\n"
               " Checksum: %s\n"
               " Raw: [%s]"
               % (hex(self.delimiter), hex(self.address), self.command, 
                self.bytecount, self.communicationstatus, self.devicestatus,
                self.responsecode,
                ' '.join('{:02x}'.format(int(x)) for x in self.data),
                hex(self.checksum), 
                ' '.join('{:02x}'.format(int(x)) for x in list(self.raw))))

class HART_StdDevice:
    port = None
    uniqueid = None
    DEBUG_HART_FRAMES = False
    DEBUG_HART_TXRX = True

    def __init__(self, comport=None):
        if comport == None:
            # Com port not given, try to autodetect a USB/serial converter
            validports = []
            for port in serial.tools.list_ports.comports():
                if "USB Serial Port" in port.description:
                    validports += [port]
            if len(validports) == 1:
                comport = validports[0].device
            elif len(validports) == 0:
                raise SerialException("Serial port not specified and "
                                      "no valid ports found")
            elif len(validports) > 1:
                raise SerialException("Serial port not specified "
                                      "and multiple valid ports found: "
                                      "%s (please specify one)" 
                                      % [i.device for i in validports])
            else:
                raise Exception("Unknown error")
            print("Autodetected port %s" % comport)
        self.port = serial.Serial()
        self.port.port = comport
        self.port.baudrate = 1200
        self.port.parity = serial.PARITY_ODD
        self.port._timeout = 1.0
        self.port._rts_state = False # Workaround bug where weirdness happens 
        self.port._dtr_state = False # if these states are not set to False 
                                     # before open()
        self._open()

    def close(self):
        if self.port.is_open:
            self.port.flush()
            self.port.close()
            
    def _open(self):
        self.port.open()
        self.port.dtr = True # DTR=ON permanently (powers the HART modem)
        
            
    def sendAndReceive(self, txframe: HARTCommandFrame) -> HARTResponseFrame:
        """
        Sends a command packet to the HART modem and listens for the repsonse.
        """
        if self.DEBUG_HART_TXRX:
            print("Command %d tx" % txframe.command)
        if self.DEBUG_HART_FRAMES:
            print(txframe)
        self.port.setRTS(True)
        self.port.write(txframe.raw)
        time.sleep(BYTE_TX_LENGTH_S * (len(txframe.raw)+1))
        self.port.flushOutput()
        self.port.setRTS(False)
        
        raw = b''
        preamblestarted = False
        for i in range(NUM_RX_PREAMBLES_MAX+1):
            c = self.port.read(1)
            if len(c) == 0:
                raise HARTRxTimeoutException("No response from remote device")
            raw += c
            # look out for start of preamble
            if not preamblestarted:
                if raw[-1] == 0xFF:
                    preamblestarted = True
            else:
                # exit if preamble has started and now finished.
                if raw[-1] != 0xFF:
                    break
        if c[-1] == 0xFF:
            raise HARTException("Preamble too long?")
        addresstype = HARTAddressType.Unique if c[-1] & 0x80 else HARTAddressType.Polling
        if c[-1] & 0b01100000:
            raise HARTNotImplementedException("Expansion bytes support not implemented")
        # Number of further bytes to read depends on addresstype
        num = 5+1+1 if addresstype == HARTAddressType.Unique else 1+1+1
        c = self.port.read(num)
        if len(c) < num:  # if we didn't get enough bytes back
            raise HARTRxTimeoutException("Incomplete frame from remote device")
        raw += c
        # last byte we just rx'd was the byte count (8 bit value), add 1 for checksum
        num = c[-1] + 1
        c = self.port.read(num)
        raw += c
        if len(c) < num:
            raise HARTRxTimeoutException("Incomplete frame from remote device")
        # we've now recieved the full HART frame.  Parse it.
        rxframe = HARTResponseFrame(raw)
        if self.DEBUG_HART_FRAMES:
            print(rxframe)
        if self.DEBUG_HART_TXRX:
            print("Command %d rx %s" % (rxframe.command, 
                "OK" if not rxframe.communicationstatus.CommunicationError 
                else rxframe.communicationstatus ))
        # If we didn't get a response code of 0
        if rxframe.responsecode is not None:
            rc = rxframe.responsecode
            if rxframe.responsecode != 0:
                # We've got problems.  Subsequent code in the calling function 
                # will probably fail because the data is probably empty.
                if rc == 64:
                    raise HARTRxCommandNotImplementedException(
                            "Non-zero response code: %s" % rc)
                elif rc == 2:
                    raise HARTRxInvalidSelectionException(
                            "Non-zero response code: %s" % rc)
                elif rc == 5:
                    raise HARTRxTooFewDataBytesException(
                            "Non-zero response code: %s" % rc)
                else:
                    print("rxframe debug: %s" % rxframe)
                    raise HARTRxException(
                            "Non-zero response code: %s"% rc)
        if rxframe.communicationstatus.CommunicationError:
            raise HARTRxException(rxframe.communicationstatus)
        return rxframe
            
    ##### Universal Commands ##############################
    def SendCommand0_ReadUniqueIdentifier(self):
        """
        Send command 0 - the usual way of making first contact with a device.
        """
        txframe = HARTCommandFrame(address=0, command=0, data=[])
        rxframe = self.sendAndReceive(txframe)
        if rxframe.communicationstatus.CommunicationError:
            raise 
        #print(rxframe)
        # Unpack the data into a named tuple.  namedtuple helps identify
        # each parameter without needing a custom class for every command.
        Cmd0Response = namedtuple("Cmd0Response", [
                "byte0",
                "ExpandedDeviceType",
                "MinPreamblesMS",
                "HARTMajorRev",
                "DeviceRev",
                "SoftwareRev",
                "HardwareRev",
                "PhysicalSignalingCode",
                "Flags",
                "DeviceID",
                "MinPreamblesSM",
                "MaxDeviceVariables",
                "ConfigChangeCounter",
                "ExtFieldDeviceStatus",
                "ManufacturerID",
                "PrivateLabelID",
                "DeviceProfile",
            ]
        )
        # Unpack data as per https://docs.python.org/3/library/struct.html
        data = rxframe.data
        if len(data) < 17:
            raise HARTRxException("Not enough data bytes in Cmd0 response. "
                    "Needed >=17, got %d" % len(data))
        hartrev = data[4]
        if hartrev < 7 and self.DEBUG_HART_TXRX:
            print("HART%d device detected" % hartrev)
        if len(data) < 22 and hartrev >=7:
            raise HARTRxException("Not enough data bytes in Cmd0 response. "
                    "Needed >=22, got %d" % len(data))
       
        parsed = Cmd0Response(
            byte0=rxframe.data[0],
            ExpandedDeviceType = struct.unpack(">H", data[1:3])[0],
            MinPreamblesMS= data[3],
            HARTMajorRev  = data[4],
            DeviceRev     = data[5],
            SoftwareRev   = data[6],
            HardwareRev   = (data[7] & 0b1111000) >> 3,
            PhysicalSignalingCode = (data[7] & 0x00000111),
            Flags         = data[8],
            DeviceID      = struct.unpack(">I", b'\0' + data[9:12])[0],
            MinPreamblesSM = data[12],
            MaxDeviceVariables = data[13],
            ConfigChangeCounter = struct.unpack(">H", data[14:16])[0],
            ExtFieldDeviceStatus = data[16],
            ManufacturerID = struct.unpack(">H", 
                    data[17:19])[0] if hartrev >=7 else None,
            PrivateLabelID = struct.unpack(">H", 
                    data[19:21])[0] if hartrev >=7 else None,
            DeviceProfile = data[21] if hartrev >=7 else None,
        )
        #construct the 38 bit uniqueID
        # [3 pad bytes][2 expanded device type bytes][3 device id bytes]
        # interpreted as an 8byte unsigned long long integer (">Q")
        uniqueid = struct.unpack(">Q", b'\0' + b'\0' + b'\0' + data[1:3] + data[9:12])[0]
        self.uniqueid = uniqueid  # save to class variable so others can use
        print("Calculated UniqueID = %s" % hex(uniqueid))
        return(parsed)
    
    def SendCommand1_ReadPrimaryVariable(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=1, data=[])
        rxframe = self.sendAndReceive(txframe)
        Cmd1Response = namedtuple("Cmd1Response", [
                "PrimaryVariableUnits",
                "PrimaryVariable",
            ]
        )
        data = rxframe.data
        parsed = Cmd1Response(
            PrimaryVariableUnits = HARTUnits(data[0]),
            PrimaryVariable = struct.unpack(">f", data[1:5])[0]
        )
        return parsed
        
    def SendCommand2_ReadLoopCurrentAndPercentOfRange(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=2, data=[])
        rxframe = self.sendAndReceive(txframe)
        Cmd2Response = namedtuple("Cmd2Response", [
                "PrimaryVariableLoopCurrent",
                "PrimaryVariableLoopCurrentPercentOfRange",
            ]
        )
        data = rxframe.data
        parsed = Cmd2Response(
            PrimaryVariableLoopCurrent = struct.unpack(">f", data[0:4])[0],
            PrimaryVariableLoopCurrentPercentOfRange = struct.unpack(">f", data[4:8])[0]
        )
        return parsed
    
    
    def SendCommand3_ReadDynamicVariables(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=3, data=[])
        rxframe = self.sendAndReceive(txframe)
        Cmd3Response = namedtuple("Cmd3Response", [
                "PrimaryVariableLoopCurrent",
                "PrimaryVariableUnitsCode",
                "PrimaryVariable",
                "SecondaryVariableUnitsCode",
                "SecondaryVariable",
                "TertiaryVariableUnitsCode",
                "TertiaryVariable"
            #    "QuaternaryVariableUnitsCode",
            #    "QuaternaryVariable",
                ]
        )
        #print(rxframe)     #For debugging
        data = rxframe.data
        if (len(data)>19):
            raise HARTNotImplementedException("HART quaternary "
                                              "variables are not suppported yet.")
        parsed = Cmd3Response(                                                  #>f comes from https://docs.python.org/3/library/struct.html
            PrimaryVariableLoopCurrent = struct.unpack(">f", data[0:4])[0],     #byte numbers come from HART spec-127
            PrimaryVariableUnitsCode = data[4],
            PrimaryVariable = struct.unpack(">f", data[5:9])[0],
            SecondaryVariableUnitsCode = data[9],
            SecondaryVariable = struct.unpack(">f", data[10:14])[0],             #Should be pressure
            TertiaryVariableUnitsCode = data[14],
            TertiaryVariable = struct.unpack(">f", data[15:19])[0]
 
          #  Uncomment if device has quaternary variables & add comma on line above
          #  QuaternaryVariableUnitsCode = data[19],
          #  QuaternaryVariable = struct.unpack (">f", data[20:24])[0]
        )
        return parsed   
    
    def SendCommand40_EnterExitFixedCurrentMode(self, current):
        """
        Set the device into fixed current output mode.
        Set the current to !0 to enter fixed current mode.
        Set to 0 to exit fixed current mode.
        """
        data = struct.pack(">f", current)
        txframe = HARTCommandFrame(address=self.uniqueid, command=40, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        Cmd40Response = namedtuple("Cmd40Response", [
                "RxFrame",
                "ActualPV",
                ]
        )
        data = rxframe.data
        parsed = Cmd40Response(
            RxFrame = rxframe,
            ActualPV = struct.unpack(">f", data[0:4])[0],
        )
        return parsed

    def SendCommand53_WriteDeviceVariableUnits(self, deviceVariableIndex: int, unit: HARTUnits):
        """
        Write the device variable units.
        Index 0 is the usually the PV, index 1 the SV, etc.
        """
        data = struct.pack(">bb", deviceVariableIndex, unit)
        txframe = HARTCommandFrame(address=self.uniqueid, command=53, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        Cmd53Response = namedtuple("Cmd53Response", [
                "RxFrame",
                "DeviceVariableIndex",
                "DeviceVariableUnits",
                ]
        )
        data = rxframe.data
        parsed = Cmd53Response(
            RxFrame = rxframe,
            DeviceVariableIndex = struct.unpack(">b", data[0:1])[0],
            DeviceVariableUnits = struct.unpack(">b", data[1:2])[0],
        )
        return parsed

      
    @staticmethod
    def elapsed32thsToHourMinSecMillis(t32):
        s, ms = divmod(t32, 32000)
        m, s = divmod(s, 60)
        h, m = divmod(m, 60)
        h = int(h)
        m=int(m)
        s=int(s)
        ms=int(ms)
        return h,m,s,ms
     
    ##### Common Practice Commands ##############################
    def SendCommand89_SetRealTimeClock(self, t:datetime=datetime.now()):
        """
        Set the date time. See spec151 section 7.57.
        @param now: a datetime object containing the time to send to the device.
            If no parameter is given, the current time is used.
        """
        print("Setting t=%s" % t)
        # harttime = no of 1/32000ths of a second since midnight.
        harttime = int((t.hour*60*60+ t.minute*60+t.second+
                        t.microsecond/1000000) * 32000)
        
        data = struct.pack(">BBBBIh",
                           int(HARTTimeSetCode.WriteDateAndTime),
                           t.day,
                           t.month,
                           t.year-1900,
                           harttime,
                           0)
        
        txframe = HARTCommandFrame(address=self.uniqueid, command=89, 
                                   data=data)
        #print(txframe)
        rxframe = self.sendAndReceive(txframe)
        Cmd89Response = namedtuple("Cmd89Response", [
                    "RxFrame",
                    "DateDay",
                    "DateMonth",
                    "DateYear",
                    "Time32ths",
                    "DateTime",
                    ]
        )
        data = rxframe.data
        print(rxframe)
        d = struct.unpack(">B", data[1:2])[0]
        mo = struct.unpack(">B", data[2:3])[0]
        y = struct.unpack(">B", data[3:4])[0]
        t32 = struct.unpack(">I", data[4:8])[0]
        (h,mn,s,ms) = self.elapsed32thsToHourMinSecMillis(t32)
        print ("{}-{}-{} {}_{}_{}".format(y,mo,d,h,mn,s))
        parsed = Cmd89Response(
                RxFrame = rxframe,
                DateYear = y,
                DateMonth = mo,
                DateDay = d,
                Time32ths = t32,
                DateTime = datetime(y+1900, mo, d, h, mn, s, ms) 
        )
        return parsed
       
    def SendCommand90_ReadRealTimeClock(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=90, data=[])
        rxframe = self.sendAndReceive(txframe)
        Cmd89Response = namedtuple("Cmd89Response", [
                    "RxFrame",
                    "DateDay",
                    "DateMonth",
                    "DateYear",
                    "Time32ths",
                    "DateTime",

                    "DateCLSDay",
                    "DateCLSMonth",
                    "DateCLSYear",
                    "TimeCLS32ths",
                    "DateTimeCLS",

                    "RTCFlags",
                    ]
        )
        data = rxframe.data
        d = struct.unpack(">B", data[0:1])[0]
        mo = struct.unpack(">B", data[1:2])[0]
        y = struct.unpack(">B", data[2:3])[0]
        t32 = struct.unpack(">I", data[3:7])[0]
        h, mn, s, ms = self.elapsed32thsToHourMinSecMillis(t32)
        print ("{}-{}-{} {}_{}_{}".format(y,mo,d,h,mn,s))
        
        d_cls = struct.unpack(">B", data[7:8])[0]
        mo_cls = struct.unpack(">B", data[8:9])[0]
        y_cls = struct.unpack(">B", data[9:10])[0]
        t32_cls = struct.unpack(">I", data[10:14])[0]
        h_cls, mn_cls, s_cls, ms_cls = self.elapsed32thsToHourMinSecMillis(t32_cls)
        parsed = Cmd89Response(
                RxFrame = rxframe,
                DateDay = d,
                DateMonth = mo,
                DateYear = y,
                Time32ths = t32,
                DateTime = datetime(y+1900, mo, d, h, mn, s),
                DateCLSDay = d_cls,
                DateCLSMonth = mo_cls,
                DateCLSYear = y_cls,
                TimeCLS32ths = t32_cls,
                DateTimeCLS = datetime(y_cls+1900, mo_cls, d_cls, h_cls, 
                                       mn_cls, s_cls, ms_cls),
                RTCFlags = struct.unpack(">B", data[14:15])[0],
        )
        return parsed
       

    def SendCommand255_Dummy(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=255, data=[])
        rxframe = self.sendAndReceive(txframe)
        print(rxframe)
#%%        
import sys
#%%
if __name__ == "__main__":
    # Allow comport to be specified on the command line
    comport = str(sys.argv[1]) if len(sys.argv) > 1 else None
    dev = HART_StdDevice(comport)
    try:
        resp = dev.SendCommand0_ReadUniqueIdentifier()
        print(resp)
        # resp = dev.SendCommand1_ReadPrimaryVariable()
        # print(resp)
        # resp = dev.SendCommand2_ReadLoopCurrentAndPercentOfRange()
        # print(resp)
        # resp = dev.SendCommand253_Dummy()
            
    finally:
        dev.close()
    