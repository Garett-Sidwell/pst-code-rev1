# -*- coding: utf-8 -*-
"""
@author: dthompson, Norgren Leeds Ltd 9/8/2020
This file implements device specific HART stack commands for the WS341 smart
Solenoid, e.g. SendCommandXX_YyyyYyyyYyyy()

It also implements some helper commands that call the HART commands
to assist with testing.
"""
if __name__ == "__main__":
    import sys
    sys.path.append("../../")
    from HART.HART_StdDevice import HART_StdDevice, HARTCommandFrame, \
                HARTInvalidParameterException, HARTRxTimeoutException, \
                HARTDate, HARTTime, HARTException, HARTResponseCode
else:
    from .HART.HART_StdDevice import HART_StdDevice, HARTCommandFrame, \
                HARTInvalidParameterException, HARTRxTimeoutException, \
                HARTDate, HARTTime, HARTException, HARTResponseCode
from enum import IntEnum
from collections import namedtuple
#from typing import List,Tuple
import struct
import time
from datetime import datetime

from serial.serialutil import to_bytes



class HART_WS341(HART_StdDevice):
    """
    Implementation of a class to communicate with WS341 Smart Solenoid 
    (HART slave device).
    """

    class PSTStatus(IntEnum): # must match pst.h
        PST_STATUS_IDLE					= 0,	# PST is idle
        PST_STATUS_AUTOTUNE_BUSY		= 1,	# PST is being tuned
        PST_STATUS_PST_BUSY				= 2,	# PST is running
        PST_STATUS_NOT_CONFIGURED		= 3,	# PST is not configured
        PST_STATUS_RECALLING			= 4,	# Recalling trace data
        PST_STATUS_STORING_ESD_TRACE	= 5,	# Storing ESD trace data
        PST_STATUS_CALIBRATING_BUSY	= 6,	# Calibrating pressure or position 
        PST_STATUS_ERASING_BUSY		= 7,	# Erasing old traces

    class PSTResultLevel(IntEnum): # must match pst.h
        PST_PASS         = 0,	# Passed test
        PST_PASS_INFO    = 1,  # Passed but some extra info about a potenial problem
        PST_PASS_WARNING = 2, 	# Passed but progressed after a waring
        PST_FAIL 	        = 3, 	# Failed test
        PST_ABORT	        = 4, 	# Aborted
        PST_NOT_RUN      = 254,# Default not run a pst
        PST_NULL         = 255 #///< Unknown result found.
    
    class ATEStatus(IntEnum): #must match ate.c
        ATE_TASK_STATE_CAL_MEASURING		= 2,	#* Measuring pressure */
        ATE_TASK_STATE_CAL_WAITING_HIGH_CAL	= 3,	#/* Waiting for Calibrate High command */
        ATE_TASK_STATE_CAL_OK				= 4,	#/* Calibration succeeded */  
        ATE_TASK_STATE_ERROR				= 8,
        ATE_TASK_STATE_ERASING				= 18,	#///< Erase in progress
        ATE_TASK_STATE_ERASE_COMPLETE		= 19,	#///< Erase complete
        
        ATE_TASK_STATE_POS_INSUFFICIENT_ADC_RANGE = 24 #///< ADC range is less than ADC_POS_MIN_SPAN

    class ATECmd(IntEnum):
        WRITE_SERIAL_NUMBERS      = 0
        READ_SERIAL_NUMBERS       = 1,
        CAL_INTERNAL_PRESSURE     = 2
        GET_STATUS                = 3,
        TEST_SOLENOID_DRIVE       = 4,
        ERASE_LOGS_AND_SETTINGS   = 5,
        FULL_ERASE                = 6,
        GET_TEMPERATURE_COMP      = 7,
        DEFAULT_TEMPERATURE_COMP  = 9,
        DISABLE_TEMPERATURE_COMP  = 9,
        TEMPERATURE_COMPENSATE_TL_PL = 10,
        TEMPERATURE_COMPENSATE_TL_PH = 11,
        TEMPERATURE_COMPENSATE_TH_PL = 12,
        TEMPERATURE_COMPENSATE_TH_PH = 13,
        MEASURE_TCOMP_START     = 14,
        MEASURE_TCOMP_GET       = 15,
        # ATE_CMD_MAX           = 16,
        CAL_EXTERNAL_PRESSURE   = 17,
        SET_ALGROITHM           = 18,
        CAL_POS                 = 19,

    def __init__(self, comport=None, autoconnect=True):
        super().__init__(comport)
        if autoconnect:
            # First hello. Also saves the unique id needed for all other commands
            self.SendCommand0_ReadUniqueIdentifier()
            
    ##### Public Commands () ##############################
    def SendCommand15(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=15, 
                                   data=None)
        rxframe = self.sendAndReceive(txframe)
        return rxframe
    def SendCommand250(self,WriteProtect,Permissions):
        data = struct.pack(">Bi",WriteProtect,Permissions)
        txframe = HARTCommandFrame(address=self.uniqueid, command=250, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe
    
    def SendCommand251(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=251, 
                                   data=None)
        rxframe = self.sendAndReceive(txframe)
        Cmd251Response = namedtuple("Cmd251Response", [
                "WriteProtect",
                "Permissions",
                "Lockstate",
            ]
        )
        parsed = Cmd251Response(
            WriteProtect = struct.unpack(">B", rxframe.data[0:1])[0],
            Permissions = struct.unpack(">i", rxframe.data[1:5])[0],
            Lockstate = struct.unpack(">B", rxframe.data[5:6])[0],
        )
        return parsed
    
        
    ##### Non-Public Commands (122-126) ##############################
    def SendCommand122_EnterExitFactoryMode(self, password=""):
        if password == "":
            data = None
        else:
            data = password.encode("ascii")
        txframe = HARTCommandFrame(address=self.uniqueid, command=122, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe

    def SendCommand123_FactoryCommand(self,
                                      cmd: ATECmd,
                                      hartSerial: int = None,
                                      pcbSerial: str = None,
                                      traySerial: str = None,
                                      CalPoint:int = None):
        if (cmd == self.ATECmd.WRITE_SERIAL_NUMBERS):
            if hartSerial == None or \
                pcbSerial == None or \
                traySerial == None:
                raise HARTInvalidParameterException("Hart, PCB & tray S/Ns "
                                                    "must all be specified.") 
            data = struct.pack(">BBBB20s20s",
                               cmd,
                               (hartSerial >> 16) & 0xff,
                               (hartSerial >> 8) & 0xff,
                               (hartSerial & 0xff),
                               bytearray(pcbSerial, "ascii"),
                               bytearray(traySerial, "ascii"))
            txframe = HARTCommandFrame(address=self.uniqueid, command=123,
                                       data=data)
            rxframe = self.sendAndReceive(txframe)
            Cmd123Response0 = namedtuple("Cmd123Response0", [
                    "rxframe",
                ]
            )            
            parsed = Cmd123Response0(
                rxframe = rxframe,
            )
            return parsed
        elif (cmd == self.ATECmd.READ_SERIAL_NUMBERS):
            data = struct.pack(">B", cmd)
            txframe = HARTCommandFrame(address=self.uniqueid, command=123,
                                       data=data)
            rxframe = self.sendAndReceive(txframe)
            Cmd123Response1 = namedtuple("Cmd123Response1", [
                    "rxframe",
                    "hartSerial",
                    "pcbSerial",
                    "traySerial",
                ]
            )     
            data = rxframe.data
            b = struct.unpack(">BBB", data[0:3])
            print(b)
            hartSerial = (b[0] << 16) + (b[1] << 8) + b[2]
            parsed = Cmd123Response1(
                rxframe = rxframe,
                hartSerial = hartSerial,
                pcbSerial = (struct.unpack("20s", data[3:23])[0]).decode("ascii"),
                traySerial = (struct.unpack("20s", data[23:43])[0]).decode("ascii"),
                
            )
            return parsed
        
        elif (cmd == self.ATECmd.ERASE_LOGS_AND_SETTINGS):
            data = struct.pack(">B", cmd)
            txframe = HARTCommandFrame(address=self.uniqueid, command=123,
                                       data=data)
            rxframe = self.sendAndReceive(txframe)
            return rxframe
        
        elif(cmd == self.ATECmd.GET_STATUS):
            data = struct.pack(">B", cmd)
            txframe = HARTCommandFrame(address=self.uniqueid, command=123,
                                       data=data)
            rxframe = self.sendAndReceive(txframe)
            data = rxframe.data
            Cmd123Response = namedtuple("Cmd123Response", [
                    "rxframe",
                    "status",
                    ]
            )
            parsed = Cmd123Response(
                rxframe = rxframe,
                status = self.ATEStatus(struct.unpack(">B", data[0:1])[0]), 
            )
            return parsed
        elif(cmd == self.ATECmd.CAL_POS):
            if CalPoint == 1:
                point = 1
            else:
                point = 0
                
            data = struct.pack(">BB", cmd, point)
            txframe = HARTCommandFrame(address=self.uniqueid, command=123,
                                       data=data)
            rxframe = self.sendAndReceive(txframe)
            data = rxframe.data
            
            return data
            
    ##### Device Specific Commands (128-253) #########################
    def SendCommand130_ReadPSTStatus(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=130, data=[])
        rxframe = self.sendAndReceive(txframe)
        #print(rxframe)
        Cmd130Response = namedtuple("Cmd130Response", [
                "status",
            ]
        )
        parsed = Cmd130Response(
            status = self.PSTStatus(struct.unpack(">B", rxframe.data[0:1])[0]),
        )
        return parsed

    #### Code added by G Sidwell, IMI CE 02/11/2021 ####
    def SendCommand132_Autotune(self, pst_percentage):
        """
        pst_percentage is a number between 10 - 85% EXCLUSIVE
        """
        data = struct.pack(">B", pst_percentage)
        txframe = HARTCommandFrame(address=self.uniqueid, command=132, data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe

    def SendCommand133_RunPST(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=133, data=[])
        rxframe = self.sendAndReceive(txframe)
        return rxframe

        ############
    
    def SendCommand134_LegacyReadPSTResult(self):
        """Legacy command, at V2 replaced with cmd 160."""
        txframe = HARTCommandFrame(address=self.uniqueid, command=134, data=[])
        rxframe = self.sendAndReceive(txframe)
        Cmd134Response = namedtuple("Cmd134Response", [
                "rxframe",
            ]
        )
        #data = rxframe.data
        parsed = Cmd134Response(
            rxframe = rxframe
        )
        return parsed
    
    def SendCommand135_ReadRawPosition(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=135, data=[])
        rxframe = self.sendAndReceive(txframe)
        #print(rxframe)
        Cmd135Response = namedtuple("Cmd135Response", [
                "Raw",
                "RawMax",
            ]
        )
        data = rxframe.data
        parsed = Cmd135Response(
            Raw = struct.unpack(">H", data[0:2])[0],
            RawMax = struct.unpack(">H", data[2:4])[0]
        )
        return parsed
    
    def SendCommand139_GetFirmwareVersion(self, requestCode:int):
        data = struct.pack(">b", requestCode)
        txframe = HARTCommandFrame(address=self.uniqueid, command=139, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        Cmd139Response = namedtuple("Cmd139Response", [
                "rxframe",
                "string",
            ]
        )  
        #print(rxframe)
        data = rxframe.data
        if len(rxframe.data) <48:
            print("Warning - insufficient data in command 139 reply.  Perhaps FWVersion has not been set?")
            string1 = ""
        else:
            string1 = (struct.unpack("48s", data[0:48])[0]).decode("ascii")
        parsed = Cmd139Response(
            rxframe = rxframe,
            string = string1
        )
        return parsed
    

    def SendCommand142_RecallTestResult(self, traceID):
        """
        Recall a trace from SPI flash to onboard memory on the device.
        This command doesn't have a data response, the command completeing 
        without errors means start of the recall was triggered ok.
        See command 134 for polling the recall progress.
        See command 145 for reading data back out over HART.
        """
        data = struct.pack(">I", traceID)
        txframe = HARTCommandFrame(address=self.uniqueid, command=142, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe
        
    
    def SendCommand143_GetMemoryStatus(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=143, data=[])
        rxframe = self.sendAndReceive(txframe)
        print(rxframe)
        Cmd143Response = namedtuple("Cmd143Response", [
                "UsedBlocks",
                "CommissioningTraceID",
            ]
        )
        data = rxframe.data
        parsed = Cmd143Response(
            UsedBlocks = struct.unpack(">i", data[0:4])[0],
            CommissioningTraceID = struct.unpack(">i", data[4:8])[0]
        )
        return parsed
        
    def SendCommand145_ReadRecordedSamples(self, startingsample, 
                                           numsamplestoread=8, 
                                           replyformat=0,
                                           stride=1):
        """
        Reads max 8 samples from the given index of the current trace.
        """
        if numsamplestoread > 8:
            raise HARTInvalidParameterException("numsamplestoread '%d' is "
                                                "invalid.  Must be <= 8")
        data = struct.pack(">bibb", replyformat, startingsample, stride, 
                           numsamplestoread)
        txframe = HARTCommandFrame(address=self.uniqueid, command=145, data=data)
        rxframe = self.sendAndReceive(txframe)
        Cmd145Response = namedtuple("Cmd145Response", [
                "Samples",
            ]
        )
        # how many data bytes did we get?
        # each sample is 2 floats i.e. 2*4=8bytes
        data = rxframe.data
        assert len(data) % 8 == 0, "len(data)-2 must be multiple of 8."
        numrxsamples = int(len(data)/8)
        samplebuf = [] # list of lists
        for i in range(numrxsamples):
            samplebuf = samplebuf + [list(struct.unpack(">ff",data[i*8:i*8+8]))]
        parsed = Cmd145Response(
                Samples = samplebuf
        )
        return parsed
        
    def SendCommand149_RestorePSTToDefaults(self,):
        data = struct.pack(">8sB", bytearray("reset123","ascii"), 0)
        txframe = HARTCommandFrame(address=self.uniqueid, command=149, 
                           data=data)
        return txframe
        
    def SendCommand149_RestoreCALToDefaults(self,):
        data = struct.pack(">8sB", bytearray("reset123","ascii"), 1)
        txframe = HARTCommandFrame(address=self.uniqueid, command=149, 
                           data=data)
        return txframe
        
    def SendCommand157_UserCalPressure(self, transaction: int, pressure: float=None):
        if pressure is None:
            data = struct.pack(">b", transaction)
        else:
            data = struct.pack(">bf", transaction, pressure)
        txframe = HARTCommandFrame(address=self.uniqueid, command=157, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe

    def SendCommand158_UserCalPosition(self, transaction: int):
        data = struct.pack(">b", transaction)
        txframe = HARTCommandFrame(address=self.uniqueid, command=158, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe


    def SendCommand160_GetResult(self):
        txframe = HARTCommandFrame(address=self.uniqueid, command=160, data=[])
        rxframe = self.sendAndReceive(txframe)
        
        print(rxframe)
        Cmd160Response = namedtuple("Cmd160Response", [
                "numsamples",
                "status",
                "result",
                "valve_movement",
                "recalledResult",
                "dateTimeValid",
                "date",
                "time",
                "recordHoursRun",
                "sampleRate",
                "traceNumber",
                "target_movement",
                "commissioningTraceID",
                "evdMinPres",
                "evdBreakoutPres",
                "evdBreakoutTime",
                "evdActuatorSpeed",
                "evdExhaustRate",
                "evdEstClosingTime",
                "evdSolenoidReactionTimeMillisecs",
                "evdStartingPressure",
                "testTemperature",
            ]
        )
        data = rxframe.data
        # unpack the binary packed data, see here:
        # https://docs.python.org/3/library/struct.html
        parsed = Cmd160Response(
            numsamples = struct.unpack(">H", data[0:2])[0],
            status = self.PSTStatus(struct.unpack(">B", data[2:3])[0]),
            result = self.PSTResultLevel(struct.unpack(">B", data[3:4])[0]),
            valve_movement = struct.unpack(">f", data[4:8])[0],
            recalledResult = struct.unpack("?", data[8:9])[0],
            dateTimeValid = struct.unpack("?", data[9:10])[0],
            date = HARTDate(struct.unpack("ccc", data[10:13])), #"DDD",
            time = HARTTime(struct.unpack("cccc", data[13:17])), #"TTT", 
            recordHoursRun = struct.unpack(">L", data[17:21])[0],
            sampleRate = struct.unpack(">H", data[21:23])[0],
            traceNumber = struct.unpack(">L", data[23:27])[0],
            target_movement = struct.unpack(">f", data[27:31])[0],
            commissioningTraceID = struct.unpack(">L", data[31:35])[0],
            evdMinPres = struct.unpack(">f", data[35:39])[0],
            evdBreakoutPres = struct.unpack(">f", data[39:43])[0],
            evdBreakoutTime = struct.unpack(">f", data[43:47])[0],
            evdActuatorSpeed = struct.unpack(">f", data[47:51])[0],
            evdExhaustRate = struct.unpack(">f", data[51:55])[0],
            evdEstClosingTime = struct.unpack(">f", data[55:59])[0],
            evdSolenoidReactionTimeMillisecs = struct.unpack(">f", data[59:63])[0],
            evdStartingPressure = struct.unpack(">f", data[63:67])[0],
            testTemperature = struct.unpack(">f", data[67:71])[0],
        )
        return parsed


    def SendCommand253_UnlockDevice(self, password):
        """
        Unlock the device
        """
        data = struct.pack(">8s", bytes(password, 'latin1'))
        txframe = HARTCommandFrame(address=self.uniqueid, command=253, 
                                   data=data)
        rxframe = self.sendAndReceive(txframe)
        return rxframe
        
    

    ##### Helper functions ###########################################
    def readPosition(self):
        """
        A helper command to read the actuator position.
        """
        resp = self.SendCommand1_ReadPrimaryVariable()
        #print(resp)
        return resp.PrimaryVariable

    def readLoopCurrent(self):
        """
        A helper command to read the loop current.
        """
        resp = self.SendCommand2_ReadLoopCurrentAndPercentOfRange()
        #print(resp)
        return resp.PrimaryVariableLoopCurrent
    
    def readSoftwareRevision(self):
        """
        A helper command to read the software revision.
        """
        resp = self.SendCommand0_ReadUniqueIdentifier()
        #print(resp)
        return resp.SoftwareRev

    def readPressure(self):
        """
        A helper command to read the pressure.
        """
        resp = self.SendCommand3_ReadDynamicVariables()
        #print(resp)
        return resp.SecondaryVariable

    def recallTrace(self, traceID):
        """
        Tells the embedded system to load a specific trace from SPI flash
        into on-chip memory.
        """
        # firstly check that the traceID is valid
        resp = self.SendCommand143_GetMemoryStatus()
        lasttrace = resp.UsedBlocks-1
        if traceID > lasttrace:
            raise HARTInvalidParameterException("Cannot recall traceID=%d, "
               "This device only has traces 0 to %d", lasttrace)
        #send the command to start recalling this trace.
        print("Recalling trace %d..." % (traceID), end="")
        self.SendCommand142_RecallTestResult(traceID)
        recalldone = False
        tstart = time.time()
        while (time.time() < tstart + 10): # timeout after 10s.
            resp = self.SendCommand160_GetResult()
            time.sleep(1)
            #print("PSTStatus=%s" % resp.status)
            if resp.status != self.PSTStatus.PST_STATUS_RECALLING:
                recalldone = True
                print("complete.")
                break
            print(".", end="")
        if not recalldone:
            raise HARTTimeoutException("Timed out waiting for device to "
                                       "return to idle after triggering "
                                       "recall test result.")
        return resp
        
    def readTrace(self, traceID=None):
        """
        Reads a trace from the device to the PC.
        traceID : int, optional
             Optionally specifies a traceID to read.  If none is given, the
             currently loaded trace is read (e.g. the most recent PST).
        """
        if traceID is not None:
            self.recallTrace(traceID)
        # Find out about the trace we are about to download.
        traceresp = self.SendCommand160_GetResult()
        
        fulltracebuf = []
        samplestotal = traceresp.numsamples
        samplestotal = 2 # during debug make this small (e.g. 12)!
        print("Reading trace...", end="")
        for i in range(0, samplestotal, 8):
            samplestoread = samplestotal - i
            if samplestoread > 8:
                samplestoread = 8
            resp = self.SendCommand145_ReadRecordedSamples(startingsample=0, 
                    numsamplestoread=samplestoread)
            #print(resp)
            fulltracebuf += resp.Samples
            print(".", end="")
        print("complete.")
        return (traceresp, fulltracebuf)

    def enterFactoryMode(self):
        resp = self.SendCommand122_EnterExitFactoryMode("PLEASE_CONTACT_DTHOMPSON_IF_YOU_NEED_THIS")
        if resp.responsecode == 2:
            raise HARTInvalidParameterException("Wrong password?")
        elif resp.responsecode != 0:
            raise HARTException("Command 122 failed: %s" %
                                resp.responsecode)
        else:
            print("EnterFactoryMode OK")

    def exitFactoryMode(self):
        resp = self.SendCommand122_EnterExitFactoryMode("")
        if resp.responsecode != 0:
            raise HARTException("Command 122 failed: %s" %
                                resp.responsecode)
        else:
            print("ExitFactoryMode OK")
        
    def setDateTime(self, time:datetime=datetime.now()):
        resp = self.SendCommand89_SetRealTimeClock(time)
        if resp.RxFrame.responsecode != 0:
            raise HARTException("Command 89 failed: %s" %
                                resp.RxFrame.responsecode)
        else:
            print("SetDateTime OK")
    
    def getDateTime(self):
        resp = self.SendCommand90_ReadRealTimeClock()
        if resp.RxFrame.responsecode != 0:
            raise HARTException("Command 90 failed: %s" % 
                                resp.RxFrame.responsecode)
        else:
            print("GetDateTime OK")
        return resp.DateTime

    def setSerialNumbers(self,
                         hartSer: int,
                         pcbSer: str,
                         traySer: str):
        resp = self.SendCommand123_FactoryCommand(self.ATECmd.WRITE_SERIAL_NUMBERS,
                                                  hartSerial = hartSer,
                                                  pcbSerial = pcbSer,
                                                  traySerial = traySer)
        if resp.rxframe.responsecode != 0:
            raise HARTException("Command 123 set serial numbers failed: %s" %
                                resp.RxFrame.responsecode)
        return resp
    
    def readSerialNumbersATE(self):
        resp = self.SendCommand123_FactoryCommand(self.ATECmd.READ_SERIAL_NUMBERS)
        if resp.rxframe.responsecode != 0:
            raise HARTException("Command 123 read serial numbers failed: %s" %
                                resp.RxFrame.responsecode)
        return {"hartSerial": resp.hartSerial,
                "pcbSerial": resp.pcbSerial.rstrip('\x00'),
                "traySerial": resp.traySerial.rstrip('\x00')
                }

    
    def readSerialNumbers(self):
        # firmware version
        resp = self.SendCommand139_GetFirmwareVersion(0)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139-0 failed: %s" % 
                                resp.rxframe.responsecode)
        fwVer = resp.string.rstrip('\x00')
        # build date
        resp = self.SendCommand139_GetFirmwareVersion(1)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139-1 failed: %s" %
                                resp.rxframe.responsecode)
        buildDate = resp.string.rstrip('\x00')
        # software copyright
        resp = self.SendCommand139_GetFirmwareVersion(2)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139-2 failed: %s" %
                                resp.rxframe.responsecode)
        swCopyright = resp.string.rstrip('\x00')
        # serialnum
        resp = self.SendCommand139_GetFirmwareVersion(3)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139-3 failed: %s" %
                                resp.rxframe.responsecode)
        serialNum = resp.string.rstrip('\x00')
        # Hart S/N ("hostID")
        resp = self.SendCommand139_GetFirmwareVersion(4)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139-4 failed: %s" %
                                resp.rxframe.responsecode)
        hostID = int(resp.string.rstrip('\x00'))
        return {"fwVer": fwVer,
                "buildDate": buildDate,
                "swCopyright": swCopyright,
                "serialNum": serialNum,
                "hostID": hostID,
                }

    def getFirmwareVersion(self):
        resp = self.SendCommand139_GetFirmwareVersion(0)
        if (resp.rxframe.responsecode != 0):
            raise HARTException("Command 139 failed: %s" % 
                                resp.rxframe.responsecode)
        return resp.string.rstrip('\x00')          

##########################################################################
# Example usage:
if __name__ == "__main__":
    import pandas as pd
    import numpy as np

    dev = HART_WS341("COM4")
    print(" ")
    try:
        mem_stat = dev.SendCommand143_GetMemoryStatus()
        # print(mem_stat)

        traceID = 0
        test_result = dev.SendCommand142_RecallTestResult(traceID=traceID)
        print(test_result)

        # read most recent trace
        trace = dev.readTrace()

        # get PST result
        result = dev.SendCommand160_GetResult()
        print('PST Result:', result)

        df_info = pd.DataFrame(result, result._fields)
        df_info.to_excel('hartlib\HART\Data\Trace_' + str(traceID) + '_data.xlsx', sheet_name='PST Info')


        # read samples from specific trace
        position = 0
        results_list = []
        while position < result.numsamples:
            print('loading.....', position , '/' , result.numsamples, '. ', (position/result.numsamples)*100, '%')
            samples = dev.SendCommand145_ReadRecordedSamples(startingsample=position)
            results_list.append(samples.Samples)
            position += 8

        df_list = []
        for i in range(len(results_list)):
            for j in range(len(results_list[i])):
                df_list.append(list(results_list[i][j]))

        results_df = pd.DataFrame(df_list, columns=['Pressure','Position'])
        results_df.insert(0,'Time', value=results_df.index*result.sampleRate/1000)

        with pd.ExcelWriter('hartlib\HART\Data\Trace_' + str(traceID) + '_data.xlsx', engine='openpyxl',mode='a') as writer:
            results_df.to_excel(writer, sheet_name='Data', index=False)


        # stop communication
        raise Exception("stopped")
        
        if False:
            #pvcur = dev.SendCommand40_EnterExitFixedCurrentMode(4.0)
            #print(pvcur)
            dev.setDateTime()
            resp = dev.getDateTime()
            print(resp)

        if True:
            print("fw='%s'" % dev.getFirmwareVersion())
            dev.enterFactoryMode()
            dev.setSerialNumbers(0x1234, "pcbserial1", "trayserial1")
            resp = dev.readSerialNumbersATE()
            print(resp)
        #resp = dev.SendCommand89_SetRealTimeClock()
        #print(resp)
        
        if False:
            resp = dev.SendCommand253_UnlockDevice("7MF7WPTB")
            print(resp)
            # read raw position
            dev.DEBUG_HART_FRAMES = False
            dev.DEBUG_HART_TXRX = False
            for i in range(10):
                posraw = dev.SendCommand135_ReadRawPosition()
                print("PositionRaw = %s/%s" % (posraw.Raw, posraw.RawMax))
            print("")
            
            # Get traces from SPI flash
            mem = dev.SendCommand143_GetMemoryStatus()
            print(mem)
            status = dev.recallTrace(0)
            trace = dev.readTrace()
            print(trace)
    
            i = 0
            for s in trace[1]:
                print("%4d: %.3f %.3f" %( i, s[0], s[1]))
                i += 1
            
            for i in range(1):
                #pos = dev.readPosition()
                #print("%d: Position = %s" % (i, pos))
                pass
            
    finally:
        dev.close()


